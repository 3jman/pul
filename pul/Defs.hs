module Defs where

import Control.Exception

data Block = Block { ref :: Ident, terms :: [Term] } deriving Show

data Term = Var Ident
  | Rule (String, String)
  | Esc String
  | Null Bool
  deriving Eq


type Ident = String

data Pul = Pul { blocks :: [Block], run :: Run, imports :: [String] }

data Run = Run { tar :: Ident, debug :: Bool } deriving (Show, Eq)

data CompState = CompState { target :: Ident
                           , fifoHead :: Term
                           , fifo :: [Term]
                           , input :: String
                           } deriving Show

data PulExceptions = ReferenceDoesNotExist String
                   | ReferenceDuplicate String
                   | ListOfErrs [PulExceptions]
                   deriving Show

instance Exception PulExceptions

instance Show Term where
  show (Rule (a,b)) = "-" ++ a ++ "-" ++ b
  show (Var id)     = ['"'] ++ id ++ ['"']
  show (Esc str)    = "@ " ++ str
  show (Null b)     = "null" ++ case b of
                                  False -> ""
                                  _ -> show b
