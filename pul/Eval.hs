module Eval where

import Defs
import Control.Applicative ((<|>), many)
import Control.Monad (when, guard, foldM)
import Control.Monad.Trans.Class (lift, MonadTrans)
import Data.List.Utils (replace)
import Data.List (isInfixOf)
import Data.Functor.Identity (Identity)
import Control.Monad.IO.Class (liftIO)
import Control.Monad.Trans.RWS.Lazy
import Control.Monad.Trans.Maybe (hoistMaybe, runMaybeT, MaybeT)
import Control.Monad.Trans.Except
import Control.Exception (Exception)
import Control.Parallel (pseq)

type PulInterpreter m = MaybeT (RWST Pul String CompState m)
type RunWrite = Identity
type DebugWrite = IO

isNotNull (Null b) = b
isNotNull _        = True

isRule (Rule _) = True
isRule _        = False

findBlock :: Ident -> [Block] -> Block
findBlock id (b:bs) = if ref b == id then b else findBlock id bs
findBlock id []     = undefined --some error will be thrown here

headMaybe (x:_) = Just x
headMaybe []    = Nothing

pushTerms :: Monad m => PulInterpreter m ()
pushTerms = lift $ do
  ident <- gets target
  b     <- asks $ (findBlock ident) . blocks
  modify (\st -> st { fifo = (terms b) ++ fifo st })

pop :: Monad m => PulInterpreter m ()
pop = do
  l <- lift $ gets fifo
  case l of
    (t:ts) -> lift $ modify $ \st -> st { fifoHead = t, fifo = ts }
    []     -> hoistMaybe Nothing
  
push :: Monad m => Term -> PulInterpreter m ()
push t = lift $ modify (\st -> st { fifo = [t] ++ (fifo st) } )

popTerms :: Monad m => PulInterpreter m ()
popTerms = lift $ modify (\st -> st { fifo = dropWhile isNotNull $ fifo st })  

  

evalTerm :: Monad m => Term -> PulInterpreter m ()
evalTerm (Var id)     = (lift $ modify (\st -> st { target = id })) >> pushTerms
evalTerm (Rule (a,b)) = lift $ modify (\st -> st { input = replace a b (input st) })
evalTerm (Esc str)    = (lift $ gets input) >>= \i -> when (str `isInfixOf` i) popTerms
evalTerm (Null value) = lift $ when value (gets input >>= tell)


stateFul :: PulInterpreter RunWrite ()
stateFul = do
  h <- lift $ gets fifoHead
  evalTerm h
  pop
  stateFul


runner pul st = evalRWST (runMaybeT stateFul) pul st >>= \(_, s) -> return s

dRunner pul st = (evalRWST (runMaybeT dstateFul) pul st)

dstateFul :: PulInterpreter IO [()]
dstateFul = many $ do
  h <- lift $ gets fifoHead
  evalTerm h
  lift get >>= \st ->
    liftIO $
    putStrLn $ "FIFO :: " ++ (show $ fifo st) ++ "TARGET :: " ++ (show h) ++ " :-:> " ++ (input st)
  pop


initState :: Run -> String -> CompState
initState r inpStr = CompState { target = tar r
                               , fifo = [Null True]
                               , input = inpStr
                               , fifoHead = Var $ tar r
                               }

eval :: Pul -> String -> IO ()
eval pul inpStr = print $ runner pul (initState (run pul) inpStr)
    

debug :: Pul -> String -> IO String
debug pul inpStr = dRunner pul (initState (run pul) inpStr) >>= \(_, s) -> return s
