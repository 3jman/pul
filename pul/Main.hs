module Main where

import Defs
import Pars
import Eval (eval, debug)
import TypeCheck (nameCheck)
import System.Environment (getArgs)
import Text.Parsec.String (parseFromFile)
import Control.Parallel (pseq)
import Control.Monad (void, foldM)
import Control.Monad.Trans.State

type IMPState = StateT Pul IO ()

actionPul pul = do
  putStrLn "ENTER INPUT"
  inp <- getLine
  handleImports eval inp pul


addImps bs = modify $ \p -> p { blocks = blocks p ++ bs }

handleImports :: (Pul -> String -> IO ()) -> String -> Pul -> IO ()
handleImports f str pul = if imports pul /= []
                          then (do
                                   bss <- pasteImports
                                   f (pul { blocks = blocks pul ++ bss }) str)
                          else f pul str
  where pasteImports = foldl1 (<>) $ map handle $ imports pul
        handle = \st -> do
          parsed <- parseFromFile pulParse st
          case parsed of
            Left err   -> return []
            Right pul0 -> nameCheck pul0 `pseq` return $ blocks pul0
          
debugAction pul = putStrLn "ENTER INPUT AND REMEMBER YOU ARE DEBUGGING" >> getLine >>= \inp -> handleImports (\p i -> void $ Eval.debug p i) inp pul
  
main = do
  l <- getArgs
  case length l of
    0 -> fileOption
    1 -> do
      parsed <- parseFromFile pulParse (head l)
      case parsed of
        Left err  -> print err
        Right pul -> nameCheck pul >> if Defs.debug $ run pul
                                      then debugAction pul
                                      else actionPul pul
    2 -> do
      parsed <- parseFromFile pulParse (head l)
      case parsed of
        Left err  -> print err
        Right pul -> nameCheck pul >> if Defs.debug $ run pul
                                      then handleImports (\p i -> void $ Eval.debug p i) inp pul
                                      else handleImports eval inp pul
                                     where inp = head $ tail l
    _ -> error "You provided too (or not) many arguments"


fileOption = do
  putStrLn "ENTER FILENAME"
  filename <- getLine
  parsed   <- parseFromFile pulParse filename
  case parsed of
    Left err  -> print err
    Right pul -> nameCheck pul `pseq` if Defs.debug $ run pul
                            then debugAction pul
                            else actionPul pul
