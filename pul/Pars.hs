module Pars where

import Eval
import Defs
import Text.Parsec
import Text.Parsec.Combinator
import Text.Parsec.Char
import Text.Parsec.String
import Text.Parsec.Expr
import Text.Parsec.Language
import Text.Parsec.Token

def = emptyDef { commentStart = "-**"
               , commentEnd = "**-"
               , commentLine = "-***"
               , identStart = letter
               , identLetter = alphaNum
               , opStart = oneOf "<>@|+"
               , opLetter = oneOf "<>@|+"
               , reservedOpNames = ["<", ">", "@", "|", "+", "$"]
               , reservedNames = ["run", "debug", "import"]
               , caseSensitive = True
               }

TokenParser { parens = m_parens
            , identifier = m_identifier
            , reservedOp = m_reservedOp
            , reserved = m_reserved
            , whiteSpace = m_whiteSpace
            , semi = m_semi
            , comma = m_comma
            , stringLiteral = m_stringLiteral
            , charLiteral = m_charLiteral
            , commaSep1 = m_commaSep1
            , symbol = m_symbol
            , integer = m_integer
            } = makeTokenParser def

parsImports :: Parser [String]
parsImports = many $ try $ m_reserved "import" >> m_stringLiteral -- <* newline

primTermParse :: Parser Term
primTermParse =(try ruleParse)
  <|> (try escParse)
  <|> (try varParse)


varParse = do
  id   <- m_identifier
  return $ Var id 
                                              
ruleParse :: Parser Term
ruleParse = do
  m_symbol "-"
  target <- many1 ((noneOf ",@- ") <|> m_charLiteral) <* m_symbol "-"
  output <- manyTill ((noneOf ",@-") <|> m_charLiteral) (try $ lookAhead $ m_comma <|> m_semi)
  return $ Rule (target, output)

escParse :: Parser Term
escParse = do
  m_reservedOp "@"
  str <- m_stringLiteral
  return $ Esc str
  
listParse :: Parser [Term]
listParse = m_reservedOp "<" *> (m_commaSep1 primTermParse) <*  m_semi

blockParse :: Parser Block
blockParse = do
  m_reservedOp ">"
  name <- m_identifier
  trms <- listParse
  option (Block { ref = name, terms = trms ++ [Null False] }) (m_semi >> return Block { ref = name, terms = trms })

runParse :: Parser Run
runParse =  (do
                try $ m_reserved "debug"
                n    <-  m_identifier
                return $ Run { tar = n, Defs.debug = True }) <|>
            (do
                try $ m_reserved "run"
                n <- m_identifier
                return $ Run { tar = n, Defs.debug = False })

pulParse :: Parser Pul
pulParse = m_whiteSpace >> handler <* eof
  where handler = do { imps <- parsImports
                     ; bloks <- manyTill blockParse (try $ lookAhead $ ((m_reservedOp "run") <|> m_reserved "debug"))
                     ; rn    <- runParse
                     ; return $ Pul { blocks = bloks, run = rn, imports = imps }
                     }
