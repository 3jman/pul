module TypeCheck where

import Defs
import Eval
import Control.Monad.Trans.Reader
import Control.Monad.IO.Class
import Control.Monad
import Control.Exception (throw)
import Control.Monad.Trans.Class (lift)
import Control.Monad.Trans.Except
import Data.Maybe (fromMaybe)
import Data.List

isolateIdent :: Term -> [Ident]
isolateIdent (Var id) = [id]
isolateIdent _        = []

getNames :: Pul -> [Ident]
getNames pul = map ref $ blocks pul


getCalls :: Pul -> [Ident]
getCalls pul = foldl1 (<>) $ map isolateIdent $ foldl1 (<>) $ map terms $ blocks pul


duplicateRefs pul = let ns = getNames pul
                    in ns \\ nub ns


nonExistCalls pul = let ns = getNames pul
                        cs = getCalls pul
                    in nub cs \\ ns



nameCheck :: Pul -> IO ()
nameCheck pul = liftIO $ case (duplicateRefs pul, nonExistCalls pul) of
    ([], []) -> putStrLn ("No duplicate references exist in " ++ (tar $ run pul))
                             >> putStrLn ("All calls point to existing references in " ++ (tar $ run pul))
    (l1, []) -> (handleDupl l1) >> putStrLn ("All calls point to existing references in " ++ (tar $ run pul))
    ([], l2) -> putStrLn ("No duplicate references exist in " ++ (tar $ run pul))
                             >> (handleExis  l2)
    (l1, l2) -> (handleDupl l1) >> (handleExis l2)
handleDupl l = if length l > 1
               then throw $ ListOfErrs $ map ReferenceDuplicate l
               else throw $ ReferenceDuplicate $ head l
handleExis l = if length l > 1
               then throw $ ListOfErrs $ map ReferenceDoesNotExist l
               else throw $ ReferenceDoesNotExist $ head l
